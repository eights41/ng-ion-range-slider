import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { IonRangeSliderModule } from "ng-lib";


@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BrowserModule,
		IonRangeSliderModule
	],
	bootstrap: [ AppComponent ]
})
export class AppModule {}
