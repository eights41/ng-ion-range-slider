import { NgModule } from "@angular/core";
import { IonRangeSliderComponent } from "./ion-range-slider.component";


@NgModule({
	exports: [ IonRangeSliderComponent ],
	declarations: [ IonRangeSliderComponent ]
})
export class IonRangeSliderModule {}
