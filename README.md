# ng-ion-range-slider
[Ion Range Slider](https://github.com/IonDen/ion.rangeSlider) now optimized for easy use as an importable Angular Module and installable using npm.
Upgraded from the original [Angular port](https://github.com/PhilippStein/ng2-ion-range-slider) by Philipp Stein.

## Demos and Sample Usage

For Demos and sample usage of this package have a look at the example folder

```
git clone git@gitlab.com:csiro-geoanalytics/npm/ng-ion-range-slider.git
cd ng-ion-range-slider
npm install
npm start
```

## Installation
```
npm install @csiro-geoanalytics/ng-ion-range-slider --save
```

### Setup scripts and styles
If you use Angular CLI, add `jquery` and `ion-range-slider` to the scripts section of `angular.json`

Ensure you use `ion-rangeslider @ >=2.3.1`

```
"build": [
{
	...
	"scripts": [
		"./node_modules/jquery/dist/jquery.min.js",
		"./node_modules/ion-rangeslider/js/ion.rangeSlider.min.js"
	]
}
```

Also add the ion-range-slider style and skin css to the styles section in your `angular.json`

```
"build": [
{
	...
	"styles": [
		"./node_modules/ion-rangeslider/css/ion.rangeSlider.min.css"
	]
}
```

### Import IonRangeSliderModule

Import the `IonRangeSliderModule` into your application module:

``` TypeScript
import { IonRangeSliderModule } from "ng-ion-range-slider";

@NgModule({
    imports: [ IonRangeSliderModule ]
})
```

### Use the ion-range-slider

Use the `ion-range-slider` directive in your components.

```html
<ion-range-slider #sliderElement
	type="double"
	[min]="myMinVar"
	max="100"
	from="28"
	from_min="10"
	from_max="30"
	from_shadow="true"
	to="40"
	to_min="40"
	to_max="90"
	to_shadow="true"
	grid="true"
	grid_num="10"
	prefix="Weight: "
	postfix=" million pounds"
	decorate_both="false"
	(onUpdate)="myOnUpdate($event)"
	(onChange)="myOnChange($event)"
	(onFinish)="myOnFinish($event)"
></ion-range-slider>
```
